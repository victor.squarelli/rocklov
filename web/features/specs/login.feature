#language: pt

Funcionalidade: Login
Sendo um usuário cadastrado
Quero acessar o sistema da Rocklov
Para que eu possa anunciar meus equipamentos musicais

@login
Cenario: Login do usuário

    Dado que acesso a página principal
    Quando submeto minhas credenciais com "victor.mantovani@gmail.com" e "654321"
    Então sou redirecionado para o Dashboard

Esquema do Cenario: Tentar Logar

    Dado que acesso a página principal
    Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
    Então vejo a mensagem de alerta: "<mensagem_output>"

    Exemplos:
    |email_input                    |senha_input    |mensagem_output                    |
    |victor.squarelli@hotmail.com   |abc123         |Usuário e/ou senha inválidos.      |
    |victor.squarelli@404.com       |123456         |Usuário e/ou senha inválidos.      |
    |victor.squarelli#tmail.com     |123456         |Oops. Informe um email válido!     |
    |                               |123456         |Oops. Informe um email válido!     |
    |victor.squarelli@hotmail.com   |               |Oops. Informe sua senha secreta!   |
